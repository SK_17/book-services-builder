create table FOO_Book (
	uuid_ VARCHAR(75) null,
	bookId LONG not null primary key,
	bookName VARCHAR(75) null,
	authorName VARCHAR(75) null,
	price DOUBLE,
	publishDate VARCHAR(75) null
);