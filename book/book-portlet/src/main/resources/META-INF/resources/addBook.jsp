<%@ include file="/init.jsp" %>

<a class="btn btn-sm  btn-primary"  href="<%= homeVieweURL.toString()%>">Home</a><br>

<aui:form method="POST" action="<%=addBookActionURL%>">
<aui:input name="bookName" type="text" ><aui:validator name="required" /></aui:input>
<aui:input name="authorName" type="text" ><aui:validator name="required" /></aui:input>
<aui:input name="price" type="text" ><aui:validator name="required" /></aui:input>
<aui:input name="publishedDate" type="date" ><aui:validator name="required" /></aui:input>
<aui:button type="submit" value="submit" />
</aui:form>

 