<%@ include file="/init.jsp" %>

<a class="btn btn-sm  btn-primary"  href="<%= homeVieweURL.toString()%>">Home</a><br>

<%

Book editBook =(Book) request.getAttribute("editBook");
if( editBook!=null){
%>
<aui:form method="POST" action="<%=editBookActionURL%>">
<aui:input name="updateBookId" type="hidden" value="<%=editBook.getBookId()%>" /> 
<aui:input name="bookName" type="text" value="<%=editBook.getBookName()%>"><aui:validator name="required" /></aui:input>
<aui:input name="authorName" type="text" value="<%=editBook.getAuthorName()%>"><aui:validator name="required" /></aui:input>
<aui:input name="price" type="text" value="<%=editBook.getPrice()%>"><aui:validator name="required" /></aui:input>
<aui:input name="publishedDate" type="date" value="<%=editBook.getPublishDate()%>"><aui:validator name="required" /></aui:input>
<aui:button type="submit" value="update" />
</aui:form>
<%}
%>