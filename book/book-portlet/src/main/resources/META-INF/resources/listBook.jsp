<%@page import="com.saintgobain.book.service.persistence.BookUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ include file="/init.jsp" %>
<%@page import ="com.saintgobain.book.model.Book" %>

<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery" %>
<%@page import ="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import ="com.saintgobain.book.service.BookLocalServiceUtil"%>
 %>
<h1> Book Details</h1>
<a class="btn btn-sm  btn-primary " href="<%= addBookViewURL%>">Add Book</a>
<a class="btn btn-sm  btn-primary" href="<%= homeVieweURL.toString()%>">Home </a>

 <%

List<Book> bookList =(List<Book>) request.getAttribute("bookList");

 if (Validator.isNotNull(bookList)) {
	
	 
	   
%>

	
<aui:form action="${searchURL}" name="fm">
        <div class="row">
            <div class="col-md-8">
                <aui:input inlineLabel="left" label="" name="keywords" placeholder="search-entries" size="256" />
            </div>
            <div class="col-md-4">
                <aui:button type="submit" value="search" />
            </div>
        </div>
    </aui:form>	


<table class="table table-light">
			
				<tr>
				    
					<th scope="col">Book</th>
					<th scope="col">Author</th>
					<th scope="col">Published Date</th>
					<th scope="col">Price</th>
					<th scope="col"></th>
					<th scope="col"></th>
				</tr>
	<c:forEach items="${bookList}" var="book">  
	<portlet:renderURL var="editBookViewURL">
             <portlet:param name="viewType" value="editBook"/>
              <portlet:param name="bookId" value="${book.getBookId()}"/>
        </portlet:renderURL>	

		<portlet:renderURL var="deleteBookViewURL">
             <portlet:param name="viewType" value="deleteBook"/>
              
        </portlet:renderURL>
        <portlet:actionURL  name = "deleteBook"var="deleteBookActionURL">
        <portlet:param name="bookId" value="${book.getBookId()}"/>
        </portlet:actionURL>
        	
				<tr>
				    
					<th scope="col">${book.getBookName()}</th>
					<th scope="col">${book.getAuthorName()}</th>
					<th scope="col">${book.getPublishDate()}</th>
					<th scope="col">${book.getPrice()}</th>
					<th><a type="button" class="btn btn-warning" href="<%=editBookViewURL%>"> <i class ="glyphicon glyphicon-edit"></i></a></th>
					<th scope="col"><a class="btn btn-danger"  href="<%=deleteBookActionURL%>"><i class ="glyphicon glyphicon-remove"></i></a></th>
					
				</tr>
				</c:forEach>
				
				<%} %>
</table>