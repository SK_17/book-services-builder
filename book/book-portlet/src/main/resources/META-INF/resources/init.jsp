<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import ="com.saintgobain.book.model.Book" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import="javax.portlet.PortletURL"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />
<portlet:renderURL  var="addBookViewURL">
<portlet:param name="viewType" value="addBook"/>
</portlet:renderURL>

<portlet:renderURL  var="listBookViewURL">
<portlet:param name="viewType" value="listBook"/>
</portlet:renderURL>

<portlet:renderURL  var="editBookViewURL">
<portlet:param name="viewType" value="editBook"/>
</portlet:renderURL>
<portlet:renderURL var="searchURL">
<portlet:param name="viewType" value="searchBook" />
</portlet:renderURL>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />
<portlet:actionURL  name = "addBook"var="addBookActionURL"></portlet:actionURL>
<portlet:actionURL  name = "editBook"var="editBookActionURL"></portlet:actionURL>

<% 
PortletURL addBookURL= renderResponse.createRenderURL();
addBookURL.setParameter("addBookViewURL", "addBook");
%>
<% 
PortletURL listBookURL = renderResponse.createRenderURL();
listBookURL.setParameter("listBookViewURL", "listCourse");
%>
<% 
PortletURL editBookURL = renderResponse.createRenderURL();
listBookURL.setParameter("editBookViewURL", "listCourse");
%>

<% 
PortletURL homeVieweURL = renderResponse.createRenderURL();
homeVieweURL.setParameter("mvcPath", "/META-INF/resources/view.jsp");
%>