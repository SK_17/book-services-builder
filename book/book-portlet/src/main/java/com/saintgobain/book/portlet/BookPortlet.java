package com.saintgobain.book.portlet;

import com.saintgobain.book.constants.BookPortletKeys;
import com.saintgobain.book.model.Book;
import com.saintgobain.book.service.BookLocalServiceUtil;
import com.saintgobain.book.service.persistence.BookUtil;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=book-portlet Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + BookPortletKeys.Book,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class BookPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		   String viewType = renderRequest.getParameter("viewType");
		
           if(Validator.isNotNull(viewType) && viewType.equals("addBook")){
			
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/addBook.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		   }
           else if(Validator.isNotNull(viewType) && viewType.equals("editBook")){
        	   String bookIdstr = renderRequest.getParameter("bookId");
        	   long bookId = Long.parseLong(bookIdstr);
        	  
        		   Book editBook;
				try {
					editBook = BookLocalServiceUtil.getBook(bookId);
					renderRequest.setAttribute("editBook", editBook);
					PortletRequestDispatcher dispatcherlistCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/editBook.jsp");
	   				   dispatcherlistCourse.include(renderRequest,renderResponse);
				} catch (PortalException e) {
					
					e.printStackTrace();
				}
       	  
		   }
           else if(Validator.isNotNull(viewType) && viewType.equals("listBook")){
				List<Book> bookList = BookLocalServiceUtil.getBooks(-1, -1);
				renderRequest.setAttribute("bookList",bookList );
				PortletRequestDispatcher dispatcherlistBook = getPortletContext().getRequestDispatcher("/META-INF/resources/listBook.jsp");
				dispatcherlistBook.include(renderRequest,renderResponse);
		   }
          else if(Validator.isNotNull(viewType) && viewType.equals("searchBook")){
        	    String keyword = ParamUtil.getString(renderRequest, "keywords");  
        	    _log.info(keyword);
        	    
        	    try{
        	    	
        	    List<Book> bookList = BookLocalServiceUtil.findByBookName(keyword);
        	    _log.info(BookLocalServiceUtil.findByBookName(keyword));
        	    _log.info(bookList);
         	    renderRequest.setAttribute("bookList",bookList );
        	    }catch(Exception e){
        	    	
        	    }
           	    
                PortletRequestDispatcher dispatcherlistCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/listBook.jsp");
				dispatcherlistCourse.include(renderRequest,renderResponse);
		   }else {
				PortletRequestDispatcher dispatchereditCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/view.jsp");
				dispatchereditCourse.include(renderRequest,renderResponse);
			}

	}
	@ProcessAction(name ="addBook")
	public void addBook(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws IOException, PortletException {
		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		String bookName = GetterUtil.get(ParamUtil.getString(actionRequest, "bookName"),StringPool.BLANK);
		String authorName = GetterUtil.get(ParamUtil.getString(actionRequest, "authorName"),StringPool.BLANK);
		String priceStr = GetterUtil.get(ParamUtil.getString(actionRequest, "price"),StringPool.BLANK);
		String publishedDateStr=GetterUtil.get(ParamUtil.getString(actionRequest,"publishedDate"),StringPool.BLANK);
		double price = Double.parseDouble(priceStr);
		try{
			LocalDate publishedDate = LocalDate.parse(publishedDateStr, formatter);
			String publish = publishedDate.toString();
			       
	        long bookId = CounterLocalServiceUtil.increment(Book.class.getName()); 
	        Book book = BookLocalServiceUtil.createBook(bookId);
			book.setBookName(bookName);
			book.setAuthorName(authorName);
			book.setPrice(price);
			book.setPublishDate(publish);
			BookLocalServiceUtil.addBook(book);
			actionResponse.setRenderParameter("viewType",
				    "listBook");
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@ProcessAction(name ="editBook")
	public void editBook(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws IOException, PortletException {
		
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
		 String oldBookId = actionRequest.getParameter("updateBookId");
		    String bookName = actionRequest.getParameter("bookName");
		    String authorName = actionRequest.getParameter("authorName");
		    String priceStr = actionRequest.getParameter("price");
		    String publishedDateStr = actionRequest.getParameter("publishedDate");
		    double price = Double.parseDouble(priceStr);
		    long bookId = Long.parseLong(oldBookId);
		    Book book = BookLocalServiceUtil.fetchBook(bookId);
		    try{
				LocalDate publishedDate = LocalDate.parse(publishedDateStr, formatter);
				String publish = publishedDate.toString();
				book.setBookName(bookName);
				book.setAuthorName(authorName);
				book.setPrice(price);
			    book.setPublishDate(publish);
				BookLocalServiceUtil.updateBook(book);
				 actionResponse.setRenderParameter("viewType",
						    "listBook");
		    }catch (Exception e) {
				e.printStackTrace();
			}
	}

	@ProcessAction(name ="deleteBook")
	public void deleteBook(ActionRequest actionRequest, ActionResponse actionResponse) 
			throws IOException, PortletException {
			 
		 String bookIdString = actionRequest.getParameter("bookId");
		    if (Validator.isNotNull(bookIdString)) {
		        try {
		            long bookId = Long.parseLong(bookIdString);
		            BookLocalServiceUtil.deleteBook(bookId);
		            actionResponse.setRenderParameter("viewType",
						    "listBook");
			        }catch (Exception e) {
					e.printStackTrace();
				}
	      }
	}
	private Log _log =LogFactoryUtil.getLog(BookPortlet.class);

}